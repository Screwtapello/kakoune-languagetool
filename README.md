LanguageTool integration for Kakoune
====================================

If you write software in Kakoune,
you might have a linter set up
to annotate your code with helpful warnings,
but if you're writing documentation
or other prose,
such tools aren't as easy to come by.

One such tool is [LanguageTool][lt],
but its default output is not compatible
with what Kakoune's `:lint` plugin expects.
kakoune-languagetool provides a wrapper script
that converts LanguageTool's output
into a form that Kakoune can use.

[lt]: https://www.languagetool.org/

Installation
============

 1. Download the "Desktop version for offline use"
    linked at the bottom of [the LanguageTool website][lt]
      - Note that it's a Java tool,
        so it also requires a Java runtime to be installed
 2. Extract it somewhere,
    and note down the full path
    to the included `languagetool-commandline.jar` file
 3. Make the directory `~/.config/kak/autoload/`
    if it doesn't already exist.

        mkdir -p ~/.config/kak/autoload

 4. If it didn't already exist,
    create a symlink inside that directory
    pointing to Kakoune's default autoload directory
    so it Kakoune will still be able to find
    its default configuration.
    You can find Kakoune's runtime autoload directory
    by typing:

        :echo %val{runtime}/autoload

    ...inside Kakoune.
 5. Put copies of `languagetool.kak` and `languagetool-wrapper.sh` inside
    the new autoload directory,
    such as by checking out this git repository:

        cd ~/.config/kak/autoload/
        git clone https://gitlab.com/Screwtapello/kakoune-languagetool.git

Configuration
=============

This plugin provides an option named `languagetool_wrapper`
which contains the path to the included wrapper script,
which you can use
when setting the `lintcmd` option
in a filetype hook.
For example,
if you want to use LanguageTool to check Markdown files,
you might add something like this to your `kakrc` file:

    hook WinSetOption filetype=markdown %{
        set window lintcmd \
            "%opt{languagetool_wrapper} path/to/languagetool-commandline.jar -l en"
    }

...where `path/to/languagetool-commandline.jar`
is the full path to the `languagetool-commandline.jar` file
that comes with the desktop offline version of LanguageTool.
The `-l en` tells LanguageTool to use the rules of English grammar,
and you can add any other options you want.
You can find out what options are available
by running the jar directly:

    java -jar path/to/languagetool-commandline.jar --help

Usage
=====

Once you have set `lintcmd` to point at the wrapper script,
you can run `:lint` as normal.
